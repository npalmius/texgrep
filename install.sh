#!/bin/bash

this_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
bin_dir="$(cygpath "$(kpsewhich --var-value=TEXMFLOCAL)" | sed 's/\r//')/miktex/bin/x64"

ln -s "$this_dir/texgrep" "$bin_dir/texgrep"

